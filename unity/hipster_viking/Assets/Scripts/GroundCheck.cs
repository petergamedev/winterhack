﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {

	public LayerMask groundLayers;
	public Transform GroundCheck1;
	bool isGrounded = false;
	public float radius;

	public bool CheckGround(){
		return isGrounded;
	}

	void FixedUpdate () {
		isGrounded = Physics2D.OverlapCircle (GroundCheck1.position, radius, groundLayers);

	}
}
