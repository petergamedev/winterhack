﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	public GameSettings gameSettings;
	public Vector3 direction = Vector3.zero;

	void Start () {
		
	}
	

	void Update () {
		transform.position += direction * Time.deltaTime;
	}
}
