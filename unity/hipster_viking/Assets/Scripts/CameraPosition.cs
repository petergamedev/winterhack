﻿using UnityEngine;
using System.Collections;

public class CameraPosition : MonoBehaviour {

	public Transform character;
	public float characterOffset = 0;

	// Use this for initialization
	void Start () {

		Vector3 worldOffset = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0, 0)) - Camera.main.ViewportToWorldPoint(new Vector3(characterOffset, 0, 0));

		Vector3 position = transform.position;
		position.x = character.transform.position.x + worldOffset.x;
		transform.position = position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
