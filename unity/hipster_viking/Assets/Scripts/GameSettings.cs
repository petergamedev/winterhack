﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour {

	public float startSpeed = 0.1f;
	public float acceleration = 0.001f;

	public float currentSpeed = 0.0f;

	// Use this for initialization
	void Start () {
		currentSpeed = startSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		currentSpeed += acceleration * Time.deltaTime;
	}

	/**
	 * 
	 */
	public float GetCurrentSpeed()
	{
		return currentSpeed;
	}
}
