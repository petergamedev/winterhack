﻿using UnityEngine;
using System.Collections;

public class Die : MonoBehaviour {

	public void Killed(GameObject killedBy){

		transform.rotation = Quaternion.Euler (0, 0, -100);
		transform.GetChild(0).gameObject.SetActive(false);
		
		if(this.gameObject.tag == "Player"){
			this.gameObject.transform.rotation = Quaternion.Euler (0, 0, -100);
			Invoke ("RestartLevel", 3);
		}
	}

	void RestartLevel(){
		Application.LoadLevel (Application.loadedLevelName);
	}
}
