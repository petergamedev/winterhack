﻿using UnityEngine;
using System.Collections;

public class menu : MonoBehaviour {

	// Trigger main scene on play click
	void OnMouseDown () {
		var item = this.name;

		if (item == "start") {
			Application.LoadLevel ("Main");
		} else {
			Application.Quit();
		}
	}
}
