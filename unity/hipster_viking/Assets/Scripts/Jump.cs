﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour {

	public Vector2 jumpForce = new Vector2(0, 50);
	private bool isGrounded;
	public GroundCheck groundCheck;
	public Animator anim;


	void Start () {
		groundCheck = transform.GetChild (0).GetComponent<GroundCheck> ();
		anim = gameObject.GetComponent<Animator> ();

	}

	void Update () {

		isGrounded = groundCheck.CheckGround ();

		if(Input.GetKeyDown(KeyCode.Space) && isGrounded == true){
			rigidbody2D.AddForce (jumpForce);

		}

		anim.SetBool("isWalking", isGrounded);

	}
}
