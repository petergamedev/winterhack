﻿using UnityEngine;
using System.Collections;

public class BGLoop : MonoBehaviour {

	public GameSettings gameSettings;
	public float tileSize = 1;
	public float speedMultiplier = 1.0f;

	private float scrollSpeed = -1.0f;
	private float minPos = 0;
	private Vector3 startPosition = Vector3.zero;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;
		minPos = startPosition.x - tileSize;
	}
	
	// Update is called once per frame
	void Update () {

		scrollSpeed = -(gameSettings.GetCurrentSpeed () * speedMultiplier);

		Debug.Log ("Scrollspeed: " + scrollSpeed);

		float newPosition = transform.position.x + scrollSpeed * Time.deltaTime;
		
		if (newPosition < minPos) {
			newPosition = startPosition.x;
		}

		transform.position = new Vector3(newPosition, startPosition.y, startPosition.z);
	}
}
