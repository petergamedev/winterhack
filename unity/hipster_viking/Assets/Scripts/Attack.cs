﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {

	public Animator anim;

	void Start () {
		anim = gameObject.GetComponent<Animator> ();
		EndAttack ();
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			anim.SetTrigger("attackTrigger");
			gameObject.transform.GetChild(1).gameObject.SetActive(true);
			Invoke("EndAttack", 0.2f);
		}
	}

	void EndAttack(){
		gameObject.transform.GetChild(1).gameObject.SetActive(false);
	}
}
