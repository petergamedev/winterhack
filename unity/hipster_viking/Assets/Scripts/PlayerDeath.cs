﻿using UnityEngine;
using System.Collections;

public class PlayerDeath : MonoBehaviour {

	public LayerMask damageLayer;
	public Transform damageCollideCheck;
	bool isDead = false;
	public float radius;
	
	public bool CheckGround(){
		return isDead;
	}

	void OnTriggerEnter(Collider other) {
		Debug.Log ("OnEnter: " + other.tag);
	}

	void FixedUpdate () {
		isDead = Physics2D.OverlapCircle (damageCollideCheck.position, radius, damageLayer);

		if (isDead) {
		
		}
	}
}
